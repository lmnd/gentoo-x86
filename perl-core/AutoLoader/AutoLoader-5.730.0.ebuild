# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=SMUELLER
MODULE_VERSION=5.73
inherit perl-module

DESCRIPTION="load subroutines only on demand"

SLOT="0"
KEYWORDS="amd64 ~ppc x86 ~x86-solaris"
IUSE=""

SRC_TEST=do
