# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MY_PN=Locale-Maketext-Fuzzy
MODULE_AUTHOR=AUDREYT
MODULE_VERSION=0.11
inherit perl-module

DESCRIPTION="Maketext from already interpolated strings"

SLOT="0"
LICENSE="CC0-1.0"
KEYWORDS="amd64 hppa ppc x86"
IUSE=""

SRC_TEST=do
