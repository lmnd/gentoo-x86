# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=JSWARTZ
MODULE_VERSION=0.15
inherit perl-module

DESCRIPTION="Log::Any -- Bringing loggers and listeners together"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~ppc ~sparc ~x86"
IUSE=""

SRC_TEST=do
