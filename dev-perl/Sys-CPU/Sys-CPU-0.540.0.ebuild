# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=MZSANFORD
MODULE_VERSION=0.54
inherit perl-module

DESCRIPTION="Access CPU info. number, etc on Win and UNIX"

SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 ~ppc-macos"
IUSE=""
