# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=MIVKOVIC
MODULE_VERSION=0.79
inherit perl-module

DESCRIPTION="Simple platform independent mailer"

LICENSE="Mail-Sendmail"
SLOT="0"
KEYWORDS="alpha amd64 hppa ia64 ~mips ppc ppc64 sparc x86"
IUSE=""
