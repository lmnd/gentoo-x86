# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Fabian Groffen <grobian@gentoo.org> (24 Jan 2011)
# unmask, on Linux this should mostly work
-=sys-fs/e2fsprogs-1.41.14

