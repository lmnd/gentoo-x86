# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# This file requires eapi 5 or later. New entries go on top.
# Please use the same syntax as in use.mask

# Andrey Grozin <grozin@gentoo.org> (19 Mar 2014)
# there is no stable dev-lisp/gcl
gcl
