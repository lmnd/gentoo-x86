# ChangeLog for profile directory
# Copyright 1999-2014 Gentoo Foundation; Distributed under the GPL v2
# $Header$
#
# This ChangeLog should include records for all changes in profiles directory.
# Only typo fixes which don't affect portage/repoman behaviour could be avoided
# here. If in doubt put a record here!

  20 Mar 2015; Mikle Kolyada <zlogene@gentoo.org> package.mask:
  perl-5.18.2 and co. unmasked.

  20 Mar 2014; Sergey Popov <pinkbyte@gentoo.org> package.mask:
  Mask vulnerable versions of net-nds/openldap, bug #424167

  20 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Remove app-emacs/{eperiodic,view-process} masks, packages have been removed.

  19 Mar 2014; Mikle Kolyada <zlogene#gentoo.org> package.mask:
  Mask app-misc/bins for removal.

  19 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Lastrite x11-misc/suxpanel wrt #500408

  19 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> use.desc:
  Don't explicitely mention sys-fs/udev in USE="udev" description as the flag
  can be used also with sys-fs/eudev and sys-apps/systemd wrt #505090 by
  Francesco Turco

  19 Mar 2014; Andreas K. Huettel <dilfridge@gentoo.org>
  -targets/server/make.defaults:
  Remove targets/server, not inherited anywhere anymore

  19 Mar 2014; Andreas K. Huettel <dilfridge@gentoo.org> -releases/10.0/eapi,
  -releases/10.0/make.defaults, -releases/10.0/package.mask,
  -releases/10.0/parent:
  Remove releases/10.0, not inherited anywhere anymore

  19 Mar 2014; Andreas K. Huettel <dilfridge@gentoo.org> eapi:
  Increase EAPI to 5

  18 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Mask new media-sound/pragha together with GTK+-3.x libxfce4ui from Xfce 4.12
  (git)

  17 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Remove app-emacs/{alt-font-menu,cperl-mode,erc,nxml-mode,u-vm-color} masks,
  packages have been removed.

  17 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Add new freetype with harfbuzz dep to multilib mask.

  17 Mar 2014; Bernard Cafarelli <voyageur@gentoo.org> package.mask:
  Remove mask on gnustep-libs/cddb after last rites, bug #501160

  16 Mar 2014; Andreas K. Huettel <dilfridge@gentoo.org>
  targets/developer/make.defaults, targets/developer/parent:
  Do not inherit the server target in the developer target anymore

  16 Mar 2014; Michał Górny <mgorny@gentoo.org>
  desc/python_single_target.desc, desc/python_targets.desc:
  Update PyPy flag descriptions.

  16 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new versions of systemd-sysv-utils as well.

  16 Mar 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Mask ruby-1.8

  15 Mar 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Cleanup package.mask

  15 Mar 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Cleanup package.mask

  15 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask net-ftp/pftpfxp for removal.

  15 Mar 2014; Alexandre Rostovtsev <tetromino@gentoo.org>
  targets/desktop/gnome/package.use:
  Enable USE=vala for gcr for =app-crypt/seahorse-3.10.2-r1

  15 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Mask new xfce4-indicator-plugin together with required GTK+-3.x version of
  libxfce4ui.

  15 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Add new version of xfce4-xkb-plugin to p.mask together with
  >=xfce-base/xfce4-settings-4.11.

  15 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Add new media-video/parole with requirement for GTK+-3 libxfce4ui to Xfce
  4.12 mask

  15 Mar 2014; Pacho Ramos <pacho@gentoo.org> arch/alpha/package.use.mask,
  arch/ia64/package.use.mask, arch/sparc/package.use.mask:
  Mask rdp USE due missing keywords, bug #504672

  14 Mar 2014; Lars Wendler <polynomial-c@gentoo.org> package.mask:
  Masked <net-fs/samba-3.6 for security reasons.

  14 Mar 2014; Michael Palimaka <kensington@gentoo.org> package.mask:
  Mask kde-misc/youtube-servicemenu for removal wrt bug #504550.

  13 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask x11-themes/gdm-themes-livecd for removal.

  13 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask dev-libs/clens for removal.

  12 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Re-unite multilib mask to avoid comments in the middle as that breaks
  app-portage/diffmask.

  12 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Update the mask for fixed ebuild and virtual.

  12 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new pypy before people start wasting time on it.

  11 Mar 2014; Ben de Groot <yngwin@gentoo.org> package.mask:
  Remove mask on >=media-libs/freetype-2.5.1 (tracker bug #493570)

  11 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Update the systemd mask to catch 211.

  11 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Mask prereleases of what will be Xfce 4.12

  11 Mar 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask app-arch/mate-file-archiver, app-editors/mate-text-editor,
  app-text/mate-document-viewer, dev-python/python-caja,
  mate-base/mate-applets, mate-extra/caja-dropbox,
  mate-extra/mate-file-manager-image-converter,
  mate-extra/mate-file-manager-open-terminal,
  mate-extra/mate-file-manager-sendto, mate-extra/mate-file-manager-share,
  media-gfx/mate-image-viewer, x11-misc/mate-menu-editor,
  net-analyzer/mate-netspeed, x11-misc/mate-notification-daemon and
  x11-themes/mate-icon-theme-faenza for MATE introduction. Removed x11-wm/marco
  mask which will be reintroduced later at or after 1.8 version bumps.

  10 Mar 2014; Aaron W. Swenson <titanofold@gentoo.org> package.mask:
  x11-misc/slimlock removed from tree. Removing mask. Project merged with
  x11-misc/slim.

  09 Mar 2014; Andrey Grozin <grozin@gentoo.org> package.mask:
  unmask dev-lisp/gcl-2.6.10

  09 Mar 2014; Alexis Ballier <aballier@gentoo.org> package.mask:
  unmask latest libdvbpsi now that vlc seems ok

  09 Mar 2014; Alexis Ballier <aballier@gentoo.org> package.mask:
  unmask flac-1.3, bug #472950

  09 Mar 2014; Alexis Ballier <aballier@gentoo.org> base/package.use.mask:
  unmask vaapi on multilib ffmpeg

  09 Mar 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask mate-base/mate-file-manager, mate-extra/mate-calc,
  mate-extra/mate-character-map, mate-extra/mate-power-manager,
  mate-extra/mate-screensaver, mate-extra/mate-sensors-applet,
  mate-extra/mate-system-monitor, mate-extra/mate-utils,
  x11-terms/mate-terminal, x11-themes/mate-backgrounds, x11-themes/mate-themes
  and x11-wm/marco for MATE introduction.

  09 Mar 2014; Tristan Heaven <tristan@gentoo.org> thirdpartymirrors:
  Remove dead lokigames mirror, bug #494852

  09 Mar 2014; Alexis Ballier <aballier@gentoo.org> package.mask:
  unmask latest libva now that its consumers should work

  09 Mar 2014; Alexis Ballier <aballier@gentoo.org> package.mask:
  mask latest libass (needs harfbuzz unmasked for multilib)

  08 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Do not mask dev-python/python-exec yet. We need to get a news item along with
  it.

  08 Mar 2014; Matt Turner <mattst88@gentoo.org>
  default/linux/package.use.mask:
  Add media-libs/mesa openmax to package.use.mask.

  08 Mar 2014; Matt Turner <mattst88@gentoo.org> package.mask:
  Remove mesa-10.0 mask, see bug #492800.

  06 Mar 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Mask further ruby18-only packages.

  06 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask dev-python/python-exec for removal.

  05 Mar 2014; Markus Meier <maekke@gentoo.org> package.mask:
  remove hugin-2014 mask regarding dev-libs/boost-1.55 unmasking, bug #492594

  05 Mar 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Mask further ruby18-only packages.

  05 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask the systemd-stable snapshot due to bugs 503470, 503472.

  05 Nar 2014; Mikle Kolyada <zlogene@gentoo.org> package.mask:
  Fix mask for perl-5.18* releases.

  05 Mar 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask mate-base/mate-panel, mate-base/mate-settings-daemon,
  net-wireless/mate-bluetooth, x11-themes/mate-icon-theme and
  x11-wm/mate-window-manager for MATE introduction.

  04 Mar 2014; Julian Ospald <hasufell@gentoo.org> license_groups:
  add PAPERS-PLEASE to EULA license group

  04 Mar 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Restore mask on www-plugins/chrome-binary-plugins:unstable.

  04 Mar 2014; Julian Ospald <hasufell@gentoo.org>
  hardened/linux/amd64/no-multilib/package.mask:
  mask games-misc/papers-please for no-multilib

  04 Mar 2014; Sergey Popov <pinkbyte@gentoo.org> package.mask:
  Unmask Boost 1.54 and 1.55

  04 Mar 2014; Alexandre Rostovtsev <tetromino@gentoo.org> package.mask:
  Mask multilib libxslt until multilib libgcrypt is unmasked (bug #480402).

  04 Mar 2014; Alexandre Rostovtsev <tetromino@gentoo.org> package.mask:
  Mask multilib pangox-compat until multilib graphite2 is unmasked (bug
  #488870).

  04 Mar 2014; Alexandre Rostovtsev <tetromino@gentoo.org>
  targets/desktop/gnome/package.use:
  Add media-libs/grilo[playlist] to gnome profile for media-sound/gnome-music

  03 Mar 2014; Pacho Ramos <pacho@gentoo.org> package.mask:
  Adjust to last bump for gdk-pixbuf

  03 Mar 2014; Alexis Ballier <aballier@gentoo.org> arch/amd64/use.mask,
  arch/x86/use.mask, base/use.mask:
  mask fma3 useflag and unmask on x86/amd64

  03 Mar 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  removed pmask for media-plugins/vdr-music-0.9.9

  03 Mar 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Mask lxde-base/lxpolkit to avoid confusion for removal at a later date.

  03 Mar 2014; Patrick Lauer <patrick@gentoo.org> package.mask:
  Remove cryptsetup mask as cryptsetup-1.6.4 doesn't segfault anymore

  02 Mar 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Move Broadcom license from EULA to BINARY-REDISTRIBUTABLE group, as discussed
  with robbat2.

  01 Mar 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/delicious for removal.

  01 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask pango & deps before committing.

  01 Mar 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask gdk-pixbuf before committing.

  28 Feb 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new emul-linux-x86-gtklibs.

  28 Feb 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new emul-linux as well.

  28 Feb 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new multilib packages before committing.

  28 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Expand MATE mask messages to be more verbose, include more details, give
  advice to early installers and help overlay users.

  28 Feb 2014; Samuli Suominen <ssuominen@gentoo.org> desc/xfce_plugins.desc:
  Introduce xfce_plugins_xmonad to get rid of USE="xfce4" in xmonad-log-applet
  as per Xfce policy to not include generic flags like USE="xfce xfce4" in
  gentoo-x86

  28 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask dev-libs/libmateweather, mate-base/mate-desktop,
  mate-extra/mate-dialogs, mate-extra/mate-polkit and x11-libs/libmatewnck for
  MATE introduction.

  28 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask app-text/mate-doc-utils and mate-base/libmatekbd for MATE introduction.

  27 Feb 2014; Pawel Hajdan jr <phajdan.jr@gentoo.org> package.mask:
  Update dev channel mask for chromium-35.

  27 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask mate-base/mate-menus for MATE introduction.

  27 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask mate-base/mate-session-manager for MATE introduction.

  27 Feb 2014; Pacho Ramos <pacho@gentoo.org>
  default/linux/alpha/13.0/package.use.stable.mask:
  USE stable mask on alpha until we are able to stabilize thunderbird, bug
  #488766

  25 Feb 2014; Michael Palimaka <kensington@gentoo.org> package.mask:
  Mask media-gfx/kgraphviewer for removal.

  25 Feb 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new systemd for testing.

  24 Feb 2014; Michael Palimaka <kensington@gentoo.org> updates/1Q-2014:
  Move sys-block/kvpm from SLOT 0 to 4.

  24 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/tnt for removal.

  24 Feb 2014; Andrey Grozin <grozin@gentoo.org> package.mask:
  Unmask dev-python/pyfeyn - added a patch to fix problems

  24 Feb 2014; Andrey Grozin <grozin@gentoo.org> package.mask:
  Mask dev-python/pyfeyn - many incompatibilities with dev-python/pyx-0.12.1-r1

  23 Feb 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/package.use.mask:
  Needed dependencies cannot be installed on ia64, bug #498638

  21 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask mate-base/mate-common for MATE introduction.

  21 Feb 2014; Vladimir Smirnov <civil@gentoo.org> package.mask:
  Unmask mail-filter/dovecot_deleted_to_trash-0.5-r1, as it's fixed.

  21 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/mairix for removal.

  21 Feb 2014; Patrick Lauer <patrick@gentoo.org> thirdpartymirrors:
  Add http option to postgresql mirrors

  20 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/http-emacs for removal.

  20 Feb 2014; Luca Barbato <lu_zero@gentoo.org> package.mask:
  New libav10 beta

  19 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Fixed a small typo that I noticed while looking this up during the QA meeting.

  19 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  Mask old revision of jikes.

  18 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Unmask eselect-enabled versions of app-editors/xemacs and app-emacs/gnuserv.

  18 Feb 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Add Sendmail-Open-Source license to MISC-FREE group.

  18 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/view-process for removal.

  18 Feb 2014; Patrick Lauer <patrick@gentoo.org> package.mask:
  Append mail-filter/dovecot_deleted_to_trash to dovecot mask

  17 Feb 2014; Jeroen Roovers <jer@gentoo.org> package.mask:
  Typo.

  17 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask new revisions of app-editors/xemacs, app-emacs/gnuserv and
  app-admin/eselect-emacs for testing.

  17 Feb 2014; Mike Gilbert <floppym@gentoo.org>
  desc/python_single_target.desc:
  Add python3_4.

  17 Feb 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Mask dev-lang/python:3.4.

  17 Feb 2014; Eray Aslan <eras@gentoo.org> package.mask:
  Security mask - bug #492494 net-mail/dovecot-2.{0,1}

  17 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Temporarily mask app-emacs/gnuserv-3.12.8-r1.

  16 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/eperiodic for removal.

  16 Feb 2014; Jeroen Roovers <jer@gentoo.org> package.mask:
  Mask net-analyzer/ethstatus (bug #501432).

  15 Feb 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Delete spurious newline in MISC-FREE license group.

  15 Feb 2014; Mikle Kolyada <zlogene@gentoo.org> license_groups:
  Add Crypt-IDEA license to MISC-FREE group.

  15 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/{alt-font-menu,cperl-mode,mell,u-vm-color} for removal.

  15 Feb 2014; Jeroen Roovers <jer@gentoo.org> desc/uwsgi_plugins.desc:
  Fix encoding (bug #501348 by Harry STARR).

  15 Feb 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Unmask systemd-sysv-utils now that are tools are moved to util-linux-ng, bug
  #430912.

  15 Feb 2014; Pacho Ramos <pacho@gentoo.org> +arch/alpha/package.use,
  +arch/ia64/package.use, +arch/sparc/package.use:
  libev isn't supported on some arches, use another default value for them
  (#499498)

  14 Feb 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  removed pmask,
  media-plugins/{vdr-xxvautotimer,vdr-skinclassic,vdr-sky,vdr-skinreel},
  packages removed from tree

  14 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/erc for removal.

  14 Feb 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Mask further ruby18-only packages.

  13 Feb 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Mask further ruby18-only packages.

  13 Feb 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Remove masks for removed packages.

  13 Feb 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Mask app-emacs/nxml-mode for removal.

  13 Feb 2014; Bernard Cafarelli <voyageur@gentoo.org> package.mask:
  Last rites for gnustep-libs/cddb, bug #501160

  12 Feb 2014; Dion Moult <moult@gentoo.org> package.mask:
  Mask x11-plugins/desklet-sudoku. Unclear license, desklets dead. (bug
  #446776)

  11 Feb 2014; Dion Moult <moult@gentoo.org> package.mask:
  Masked www-client/htmlview as deprecated due to xdg-open and co (bug #480522)

  11 Feb 2014; Paul Varner <fuzzyray@gentoo.org> desc/uwsgi_plugins.desc:
  Remove Unicode character from uwsgi_plugins.desc to work around bug 498748
  for equery.

  10 Feb 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/package.use.mask,
  arch/powerpc/package.use.mask, arch/sparc/package.use.mask:
  Fix broken deps, bug #499722

  10 Feb 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/package.use.mask,
  arch/powerpc/package.use.mask, arch/sparc/package.use.mask:
  Mask USE flag due missing keywords, bug #495254

  10 Feb 2014; Dion Moult <moult@gentoo.org> package.mask:
  Add rox-extra/comicthumb to package.mask for removal in 30 days (bug #471548)

  08 Feb 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-amarok pmask removal 08/Mar/2015, wrt bug 424255

  08 Feb 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  removed several pmask for removed media-plugins/vdr-*

  07 Feb 2014; Patrick Lauer <patrick@gentoo.org> package.mask:
  Mask libgcrypt-1.6.1: Reliably makes cryptsetup segfault

  06 Feb 2014; Bernard Cafarelli <voyageur@gentoo.org> updates/1Q-2014:
  Move net-misc/mirall to net-misc/owncloud-client

  05 Feb 2014; Sergey Popov <pinkbyte@gentoo.org> package.mask:
  Remove obsolete mask on dev-python/python-irclib

  05 Feb 2014; Sergey Popov <pinkbyte@gentoo.org> package.mask:
  Unmask dev-libs/jthread

  04 Feb 2014; Sébastien Fabbro <bicatali@gentoo.org> package.mask:
  Unmasked sci-astronomy/casacore, remasked sci-astronomy/casa-data live
  ebuild

  04 Feb 2014; Lars Wendler <polynomial-c@gentoo.org>
  arch/amd64/no-multilib/package.mask:
  Removed virtualbox mask (bug #476324). arch/amd64/no-multilib/package.mask

  04 Feb 2014; Tim Harder <radhermit@gentoo.org> package.mask:
  Mask =media-sound/lilypond-2.19* for testing.

  03 Feb 2014; Tom Wijsman <TomWij@gentoo.org> package.mask:
  sys-kernel/module-rebuild removal (pending 1st February 2014), see bug
  #410739 for reference.

  03 Feb 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Mask the other dev-python/moz* packages.

  02 Feb 2014; Pacho Ramos <pacho@gentoo.org>
  default/linux/alpha/13.0/use.stable.mask,
  default/linux/ia64/13.0/use.stable.mask,
  default/linux/sparc/13.0/use.stable.mask:
  Remove stable mask for USE systemd on arches with it already in stable

  31 Jan 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Add CC-BY-SA-4.0 license to MISC-FREE-DOCS group.

  31 Jan 2014; Jeroen Roovers <jer@gentoo.org> package.mask:
  Remove beta net-analyzer/tcpreplay mask.

  30 Jan 2014; Ian Stakenvicius (_AxS_) <axs@gentoo.org> package.mask:
  drop mask of multilib gettext

  30 Jan 2014; Aaron W. Swenson <titanofold@gentoo.org> package.mask:
  Mask x11-misc/slimlock for removal.

  29 Jan 2014; Gilles Dartiguelongue <eva@gentoo.org> package.mask:
  Lift Gnome 3.10 mask after bug #499512 resolution.

  29 Jan 2014; Ian Stakenvicius <axs@gentoo.org> package.mask:
  adjust libav p.mask for recently added multilib-build ebuilds

  29 Jan 2014; Jeroen Roovers <jer@gentoo.org> package.mask:
  Drop net-misc/socat-2 mask.

  29 Jan 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Mask dev-python/mozrunner.

  29 Jan 2014; Naohiro Aota <naota@gentoo.org> package.mask:
  Mask prime and its related packages. #464286

  29 Jan 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Remove dev-ruby/mkrf mask.

  28 Jan 2014; Andreas K. Huettel <dilfridge@gentoo.org> package.mask:
  Remove obsolete libreoffice-bin-debug mask

  27 Jan 2014; Ian Stakenvicius <axs@gentoo.org>
  targets/systemd/package.use.mask:
  add sys-fs/dmraid[static] to systemd package.use.mask since it cant be built
  without lvm2[static-libs]

  27 Jan 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Append mask for >=oxygen-gtk-1.3.3:3 because it depends on gtk+-3.10

  27 Jan 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Clean up ruby18 mask.

  27 Jan 2014; Dion Moult <moult@gentoo.org> package.mask:
  Mask dev-libs/jrtplib and dev-libs/jthread (bug 489550)

  27 Jan 2014; Mike Gilbert <floppym@gentoo.org> desc/grub_platforms.desc:
  Add xen to GRUB_PLATFORMS.

  27 Jan 2014; Tim Harder <radhermit@gentoo.org> package.mask:
  Unmask =media-libs/portaudio-19_pre20140121_rc.

  26 Jan 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Mask dev-python/certifi.

  26 Jan 2014; Tim Harder <radhermit@gentoo.org> package.mask:
  Mask =media-libs/portaudio-19_pre20140121_rc until upstream resolves again
  (bug #499292).

  25 Jan 2014; Julian Ospald <hasufell@gentoo.org> package.mask:
  fixed sandbox violation wrt #499020

  25 Jan 2014; Julian Ospald <hasufell@gentoo.org> package.mask:
  hardmask >=sci-visualization/paraview-4.1.0 wrt #499020

  25 Jan 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Add Mini-XML license to MISC-FREE group.

  24 Jan 2014; Tim Harder <radhermit@gentoo.org> package.mask:
  Unmask =dev-libs/libtasn1-3*.

  24 Jan 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Mask >=media-libs/jpeg-9 and roll ~arch back from .so.9 to .so.8 due to too
  many issues being open at the Tracker wrt #479818

  24 Jan 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Clean up further obsolete ruby18 masks.

  23 Jan 2014; Ulrich Müller <ulm@gentoo.org> updates/1Q-2014:
  Update package move for games-board/capicity.

  23 Jan 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Add man-pages-posix-2013 license to MISC-FREE-DOCS group.

  23 Jan 2014; Julian Ospald <hasufell@gentoo.org> +updates/1Q-2014:
  pkgmove of games-board/capitalism to games-board/CapiCity

  22 Jan 2014; Samuli Suominen <ssuominen@gentoo.org> package.mask:
  Remove mask for still work-in-progress bash-completion layout that was
  temporarily removed from tree.

  21 Jan 2014; Ryan Hill <dirtyepic@gentoo.org> default/linux/make.defaults:
  Drop mudflap from default USE.

  20 Jan 2014; Ben de Groot <yngwin@gentoo.org> desc/linguas.desc:
  Add linguas needed for qupzilla

  20 Jan 2014; Michael Palimaka <kensington@gentoo.org> package.mask:
  Remove obsolete KDE masks since packages are now removed.

  20 Jan 2014; Pacho Ramos <pacho@gentoo.org> profiles.desc:
  Replace tabs by spaces (thanks to vapier for noticing)

  19 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/sparc/package.use.force,
  arch/sparc/package.use.mask, arch/sparc/use.mask,
  +default/linux/sparc/13.0/desktop/gnome/systemd/parent,
  +default/linux/sparc/13.0/desktop/kde/systemd/parent,
  default/linux/sparc/13.0/eapi, +default/linux/sparc/13.0/use.stable.mask,
  profiles.desc:
  systemd is now keyworded on sparc

  19 Jan 2014; Ulrich Müller <ulm@gentoo.org> package.mask:
  Remove mask for app-editors/emacs-vcs. The live ebuild will switch to empty
  keywords instead.

  19 Jan 2014; Jeroen Roovers <jer@gentoo.org> thirdpartymirrors:
  Remove Easynet (bug #498536 by Helmut Jarausch).

  19 Jan 2014; Mike Gilbert <floppym@gentoo.org> package.mask:
  Mask current google-chrome slots, bug 498306.

  18 Jan 2014; Eray Aslan <eras@gentoo.org> package.mask:
  Unmask mail-mta/postfix-2.11 and mask mail-mta/postfix-2.12

  18 Jan 2014; Alexis Ballier <aballier@gentoo.org> package.mask:
  mask latest aubio

  18 Jan 2014; Hans de Graaff <graaff@gentoo.org> package.mask:
  Remove mask for Rails 2.3 now that the packages have been removed.

  18 Jan 2014; Pawel Hajdan jr <phajdan.jr@gentoo.org> package.mask:
  Update dev channel mask for chromium-34

  16 Jan 2014; Jeroen Roovers <jer@gentoo.org> desc/fftools.desc:
  Spelling.

  16 Jan 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask multilib libintl as well.

  16 Jan 2014; Lars Wendler <polynomial-c@gentoo.org> package.mask:
  Multilib ready gettext (bug #496218). Collides with emul-linux-x86-baselibs.

  15 Jan 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask poor media-gfx/exiv2 multilib conversion.

  14 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-xxvautotimer masked for removal ~ 14/02/2014, wrt bug
  141181

  14 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-skinclassic masked for removal ~14/02/2014, wrt bug 414887

  14 Jan 2014; Alexis Ballier <aballier@gentoo.org> arch/amd64/use.mask,
  arch/x86/use.mask, base/use.mask:
  Add some usemasks for cpu related useflags that will be added to
  media-video/ffmpeg. Unmask relevant flags on amd64/x86. Leave arm and mips
  useflags to the arch team that know better where to unmask them.

  14 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-sky masked for removal ~ 14/02/2014, wrt bug 420869

  14 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-skinreel masked for removal ~ 14/02/2014, wrt bug 420873

  14 Jan 2014; Andreas K. Huettel <dilfridge@gentoo.org>
  targets/desktop/kde/package.mask:
  Unmask nvidia-drivers-331.38 since the signal mask bug is hopefully fixed
  there

  14 Jan 2014; Justin Lecher <jlec@gentoo.org> package.mask:
  Drop mask as patches apply now

  13 Jan 2014; Tony Vroon <chainsaw@gentoo.org> package.mask:
  Mask off Asterisk 12 as the first ebuild goes in.

  13 Jan 2014; Justin Lecher <jlec@gentoo.org> package.mask:
  Mask aufs-sources:3.10.26 because aufs3 patches do not apply correctly

  13 Jan 2014; Manuel Rüger <mrueg@gentoo.org> package.mask:
  Cleanup pmasked ruby packages.

  12 Jan 2014; Markus Meier <maekke@gentoo.org> package.mask:
  mask media-libs/libpano13 and media-gfx/hugin for testing

  11 Jan 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Link the license issue bug to m64py mask.

  11 Jan 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Unmask llvm-3.4 and rev-deps.

  11 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/alpha/package.use.mask,
  arch/ia64/package.use.mask, arch/powerpc/package.use.mask:
  Drop obsolete masking for systemd in dracut on arches that now have systemd

  11 Jan 2014; Pacho Ramos <pacho@gentoo.org>
  arch/powerpc/ppc64/package.use.mask:
  Drop obsolete USE maskings on ppc64 as systemd is stable there for a long
  time

  11 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/package.use.mask:
  Drop unneeded use mask

  10 Jan 2014; Markos Chandras <hwoarang@gentoo.org> thirdpartymirrors:
  Update openssl mirrors thanks to Ben Kohler <bkohler@gmail.com>. Bug #494866

  10 Jan 2014; Magnus Granberg <zorry@gentoo.org> package.mask:
  Mask sys-devel/gcc-4.8.2-r1 has default ssp and need testing

  10 Jan 2014; Justin Lecher <jlec@gentoo.org> package.mask:
  Mask +sys-kernel/aufs-sources:3.12.7 because aufs3 patches do not apply
  cleanly onto linux-3.12.7

  10 Jan 2014; Justin Lecher <jlec@gentoo.org> package.mask:
  Drop obsolete mask

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  dev-php/Text_Highlighter still has a reverse dep, unmask again

  08 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/alpha/package.use.mask:
  systemd[pam] cannot be provided for alpha, bug #438368

  08 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/alpha/use.mask,
  arch/ia64/use.mask, arch/powerpc/use.mask:
  Unmask systemd USE on arches with it keyworded (thanks to Emeric for
  noticing)

  08 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-premiereepg removal ~ 08/Feb/2014

  08 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-softplay masked for removal

  08 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-softdevice, masked for removal, #420859

  08 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-reelchannelscan, masked for removal, #420881

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PHPUnit_MockObject, dev-php/PHPUnit_Selenium for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-Tree for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-Text_Highlighter for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-Structures_DataGrid for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-HTTP_WebDAV_Server for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-HTTP_Upload for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-HTML_QuickForm_ElementGrid for removal

  08 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Mask dev-php/PEAR-HTML_BBCodeParser for removal

  08 Jan 2014; Joerg Bornkessel <hd_brummy@gentoo.org> package.mask:
  media-plugins/vdr-dvdconvert masked for removal ~08/Feb/2014, wrt bug 424281


  07 Jan 2014; Tomáš Chvátal <scarabeus@gentoo.org> package.mask:
  kdesdk-strigi-analyzer was NOT discontinued, so do not mask anymore.

  07 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/package.use.force,
  arch/ia64/package.use.mask:
  Drop no longer needed USE force on ia64

  07 Jan 2014; Pacho Ramos <pacho@gentoo.org> arch/ia64/use.mask,
  +default/linux/ia64/13.0/desktop/gnome/systemd/parent,
  +default/linux/ia64/13.0/desktop/kde/systemd/parent, profiles.desc:
  Add ia64 profiles (#478076)

  06 Jan 2014; Bernard Cafarelli <voyageur@gentoo.org> package.mask:
  Add dragonegg to llvm-3.4 mask

  06 Jan 2014; Michał Górny <mgorny@gentoo.org> package.mask:
  Mask new LLVM for testing.

  06 Jan 2014; Chí-Thanh Christopher Nguyễn <chithanh@gentoo.org>
  package.mask:
  p.mask recent libclc snapshot which needs >=llvm-3.4

  06 Jan 2014; Matti Bickel <mabi@gentoo.org> package.mask:
  Remove masks for tree-cleaned dev-php/DBUnit, dev-php/PEAR-File_PDF

  05 Jan 2014; Andreas K. Huettel <dilfridge@gentoo.org>
  targets/desktop/kde/package.mask:
  Make nvidia-drivers mask more stringent, since according to user reports also
  319.76 seems to be affected

  05 Jan 2014; creffett <creffett@gentoo.org> default/linux/package.use.mask:
  Remove invalid entries from default/linux/package.use.mask

  04 Jan 2014; Alexandre Rostovtsev <tetromino@gentoo.org>
  targets/desktop/gnome/package.use:
  Do not enable USE=gnome for verbiste and im-ja to avoid blocking gnome-3
  update.

  04 Jan 2014; Alexandre Rostovtsev <tetromino@gentoo.org>
  targets/desktop/gnome/package.use:
  Do not enable USE=gnome for lablgtk by default; it blocks the gnome-3 update
  (bug #496722).

  03 Jan 2014; Julian Ospald <hasufell@gentoo.org> use.desc:
  clarify static-libs description

  03 Jan 2014; Ulrich Müller <ulm@gentoo.org> license_groups:
  Remove unused ANTLR license, bug 496690.

  03 Jan 2014; creffett <creffett@gentoo.org>
  arch/powerpc/ppc64/package.use.mask:
  Remove invalid entries from arch/powerpc/ppc64/package.use.mask

  03 Jan 2014; creffett <creffett@gentoo.org> prefix/darwin/package.use.mask:
  Remove invalid entries from prefix/darwin/package.use.mask

  03 Jan 2014; creffett <creffett@gentoo.org>
  prefix/windows/interix/package.use.mask:
  Remove invalid entries from prefix/windows/interix/package.use.mask

  03 Jan 2014; creffett <creffett@gentoo.org> arch/arm/package.mask:
  Remove invalid entries from arch/arm/package.mask

  03 Jan 2014; creffett <creffett@gentoo.org>
  arch/amd64/no-multilib/package.mask:
  Remove invalid entries from arch/amd64/no-multilib/package.mask

  03 Jan 2014; creffett <creffett@gentoo.org> arch/amd64/x32/package.use.mask:
  Remove invalid entries from arch/amd64/x32/package.use.mask

  02 Jan 2014; William Hubbs <williamh@gentoo.org>
  prefix/darwin/macos/package.mask, prefix/package.mask,
  prefix/sunos/solaris/package.mask:
  qa: remove obsolete masks

  02 Jan 2014; William Hubbs <williamh@gentoo.org>
  features/64bit-native/package.mask:
  qa: remove obsolete masks

  02 Jan 2014; William Hubbs <williamh@gentoo.org> package.mask:
  qa: remove obsolete masks

  02 Jan 2014; Pacho Ramos <pacho@gentoo.org> package.mask:
  Mask because breaks reverse dependencies and upstream don't want us to use it
  (#483562)

  02 Jan 2014; Hans de Graaff <graaff@gentoo.org> base/use.mask:
  Remove incomplete and undocumented attempt to mask ruby18 to fix
  package-specific bugs.

  01 Jan 2014; Tim Harder <radhermit@gentoo.org> package.mask:
  Remove old lilypond development release mask.

  01 Jan 2014; Mikle Kolyada <zlogene@gentoo.org> package.mask:
  Remove obsolete perl-5.8*/perl-5.10* mask

  For previous entries, please see ChangeLog-2013.
