# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# PLEASE DO NOT ADD NEW ENTRIES HERE. 
# Use profiles/base/package.use.stable.mask or another appropriate file instead
# PLEASE DO NOT ADD NEW ENTRIES HERE. 

# Ian Delaney <idella4@gentoo.org> (09 Dec 2013)
# subsequent to the total inaction of the maintainer of
# ocaml to honour / respond in any way to 
# Bug #486076, ocaml masked to allow xen to once again
# become a stabilisable package
app-emulation/xen-tools ocaml

# Pacho Ramos <pacho@gentoo.org> (17 Nov 2013)
# Still needs gnutls-3
>=net-nntp/pan-0.139 ssl

# Sergey Popov <pinkbyte@gentoo.org> (28 Oct 2013)
# sys-cluster/cman is still in ~arch.
sys-cluster/pacemaker cman

# Mike Gilbert <floppym@gentoo.org> (21 Sep 2013)
# sys-fs/zfs isn't going stable anytime soon.
sys-boot/grub libzfs

# Doug Goldstein <cardoe@gentoo.org> (12 Sep 2013)
# Waiting on glusterfs maintainers in bug #484016
app-emulation/qemu glusterfs

# Pacho Ramos <pacho@gentoo.org> (07 Sep 2013)
# It requires newer emul sets to be stabilized (#477182#c5)
~sci-libs/fftw-3.3.3 abi_x86_32

# Agostino Sarubbo <ago@gentoo.org> (31 Aug 2013)
# Clang is not stable
app-portage/eix clang

# Samuli Suominen <ssuominen@gentoo.org> (01 Aug 2013)
# The dependencies for these flags are still in ~arch.
sys-fs/lvm2 clvm cman

# Samuli Suominen <ssuominen@gentoo.org> (12 Apr 2013)
# Waiting for >=app-cdr/brasero-3 stabilization
app-pda/gtkpod cdr

# Pacho Ramos <pacho@gentoo.org> (01 Apr 2013)
# Needed to stabilize anjuta-3.6
=dev-util/devhelp-3.6.1-r1 gedit

# Sergey Popov <pinkbyte@gentoo.org> (08 Jun 2013)
# Mask unstable dependencies in leechcraft metapackage
app-leechcraft/leechcraft-meta unstable
