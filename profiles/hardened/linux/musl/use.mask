# Copyright 1999-2014 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2
# $Header$

pam

emul-linux-x86

-elibc_musl
elibc_uclibc
elibc_glibc

-hardened
