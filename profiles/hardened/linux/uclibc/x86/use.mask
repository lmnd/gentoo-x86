# Copyright 1999-2013 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License, v2
# $Header$

# Unmask the flag which corresponds to ARCH.
-x86

# Unmask the flag corresponding to the only ABI.
-abi_x86_32

# unmask all SIMD assembler flags
-mmx
-mmxext
-sse
-sse2
-sse3
-sse4
-sse4a
-ssse3
-3dnow
-3dnowext

# Masked on all profiles but x86, bug #458354
-video_cards_geode
