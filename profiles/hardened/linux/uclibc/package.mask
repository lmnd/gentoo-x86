# Copyright 1999-2014 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2
# $Header$

sys-libs/glibc
#
sys-libs/pam
#
# Working on it, bug #470884
dev-libs/elfutils
