# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/games-fps/quake3-bin/files/q3ded.conf.d,v 1.1.1.1 2005/11/30 09:39:43 chriswhite Exp $

q3_OPTS="+set com_hunkmegs 24 +set dedicated 1 +set net_port 27960 +map q3tourney2"

