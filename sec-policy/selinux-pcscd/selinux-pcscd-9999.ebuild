# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$
EAPI="4"

IUSE=""
MODS="pcscd"
BASEPOL="9999"

inherit selinux-policy-2

DESCRIPTION="SELinux policy for pcscd"

KEYWORDS=""
