# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

DESCRIPTION="Virtual for File-Path"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="amd64 ~ia64 ~ppc ~sparc x86 ~ppc-aix"
IUSE=""

RDEPEND="~perl-core/File-Path-${PV}"
