# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2         
# $Header: ./gentoo-x86-cvsroot/net-analyzer/nagios-core/files/conf.d,v 1.1.1.1 2005/11/30 10:12:19 chriswhite Exp $

# Distributed monitoring users will want to configure here the ip/hostname of the central server.
# It will be used by submit_check_result_via_nsca.
NAGIOS_NSCA_HOST="localhost"



