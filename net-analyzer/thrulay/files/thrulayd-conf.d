# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/net-analyzer/thrulay/files/thrulayd-conf.d,v 1.1.1.1 2005/11/30 10:12:15 chriswhite Exp $

# The default window size is 4194304 bytes
#THRULAYD_WINDOW="4194304"

# By default, thrulayd will listen on 5003/tcp
#THRULAYD_PORT="5003"
