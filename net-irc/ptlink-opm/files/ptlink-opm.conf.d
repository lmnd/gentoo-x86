# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/net-irc/ptlink-opm/files/ptlink-opm.conf.d,v 1.1.1.1 2005/11/30 09:49:04 chriswhite Exp $

# User to run ptlink-opm as
PTLINKOPM_USER="ptlink-opm"
